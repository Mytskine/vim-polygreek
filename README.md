polygreek.vim
=============

Convert ASCII to polytonic greek (ancient greek).

Install in Vim manually, or with a package manager,
		or simply `:source .../plugin/polygreek.vim`.


Full examples
------------

### Normal mode

```
:call Polygreek_input()<ENTER>
hham''jr<ENTER>
<ENTER>
```
This will input `ἀμὴρ`.

Hitting Enter will convert the input into greek.
You can then accept it by hitting Enter again, or amend it.

### Insert mode

Type Alt-g (ESC-g) to convert the previous word
(or the current word, provided you are not on the first character).


Syntax
------

### Letters

```
a b g d e z j th i k l m n x o p r s t u f ch ps w
α β γ δ ε ζ η θ  ι κ λ μ ν ξ ο π ρ ς τ υ φ χ  ψ  ω
```

* Uppercase letters are available.
* β and κ have variants ϐ and ϰ, which result from post-fixing `=`. E.g. `ab=w` for αϐω.
* Any `s` at the end of a word becomes `ς`.

### Diacritics

* `hh` before a letter will add a smooth breathing (᾿). E.g. `hha` becomes ἀ, `hhi` for ἰ, etc.
* `h` will add a rough breathing (῾).
* `'` before a letter will add an acute accent (´). E.g. `'a` becomes ά, `hh'E` for Ἔ, etc
* `''` will add an grave accent.
* `^` or `~` will add a circumflex accent. E.g. `~a` becomes ᾶ.
* `"` will add a trema. E.g. `"i` becomes ϊ.

Breathing are to be put first, though it should work in any order.
See `hh''wps`, `''hhwps`, `hh''ωψ` or `hhὼψ` for ὢψ.

I ignore *oxia* and use only *tonos* (tonic accent).
There are different Unicode for each, e.g. ΐ and ΐ,
but they are usually visually identical,
and those characters are rare.

### Samples

* ὀξύβαρυς hhoz'ubarus
* ὰμφιθέατρον hhamfith'atrov
* μορφὴ morf''j
* Ἀμνηστία hhAmnjst'ia

See the `Polygreek_autotest` function for more, `:call Polygreek_autotest()`.
